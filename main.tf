resource "aws_vpc" "this" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "vpc1"
  }
}

resource "aws_subnet" "sn1" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "sn1"
  }
}

resource "aws_subnet" "sn2" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "sn2"
  }
}

resource "aws_subnet" "sn3" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "sn3"
  }
}

resource "aws_subnet" "sn4" {
  vpc_id     = aws_vpc.this.id
  cidr_block = "10.0.3.0/24"

  tags = {
    Name = "sn4"
  }
}